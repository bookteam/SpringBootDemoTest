package net.ftzcode.config

import net.ftzcode.component.JsonMethodArgumentResolver
import org.springframework.context.annotation.Configuration
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import java.nio.charset.Charset
import org.springframework.http.converter.HttpMessageConverter
import java.util.ArrayList
import com.alibaba.fastjson.serializer.SerializerFeature
import com.alibaba.fastjson.support.config.FastJsonConfig
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter
import org.springframework.http.MediaType


/**
 * Created by MAC on 2017/5/8.
 * SpringMVC之前xml配置文件  可以实在在这里全部配置！
 */


@Configuration
open class MyWebMvcConfig : WebMvcConfigurerAdapter() {

    //添加一个controller的方法自定义参数解析器 这里设置了 用 gson 解析前端请求的json数据并绑定到方法参数
    override fun addArgumentResolvers(argumentResolvers: MutableList<HandlerMethodArgumentResolver>?) {
        super.addArgumentResolvers(argumentResolvers)
        argumentResolvers?.add(JsonMethodArgumentResolver())
    }

    // 使用阿里 FastJson 作为JSON MessageConverter

/*    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>?) {
        val config = FastJsonConfig()
       *//* config.setSerializerFeatures(SerializerFeature.WriteMapNullValue, // 保留空的字段
                SerializerFeature.WriteNullStringAsEmpty, // String null -> ""
                SerializerFeature.WriteNullNumberAsZero, // Number null -> 0
                SerializerFeature.DisableCircularReferenceDetect // 禁止循环引用$ref，原样输出对象
        )*//*
        config.dateFormat = "yyyy-MM-dd HH:mm:ss"

        val converter = FastJsonHttpMessageConverter()
        converter.fastJsonConfig = config
        converter.defaultCharset = Charset.forName("UTF-8")
        val list = ArrayList<MediaType>()
        list.add(MediaType.APPLICATION_JSON_UTF8)
        converter.supportedMediaTypes = list
        converters!!.add(converter)
    }*/


    // 使用阿里 FastJson 作为JSON MessageConverter

// 使用阿里 FastJson 作为JSON MessageConverter

    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>?) {
        val converter = FastJsonHttpMessageConverter()
        val config = FastJsonConfig()
        config.setSerializerFeatures(SerializerFeature.WriteMapNullValue, // 保留空的字段
                SerializerFeature.WriteNullStringAsEmpty, // String null -> ""
                SerializerFeature.WriteNullNumberAsZero, // Number null -> 0
                SerializerFeature.WriteEnumUsingName
               // SerializerFeature.DisableCircularReferenceDetect // 禁止循环引用$ref，原样输出对象
        )
        config.dateFormat = "yyyy-MM-dd HH:mm:ss"
        converter.fastJsonConfig = config
        converter.defaultCharset = Charset.forName("UTF-8")
        val list = ArrayList<MediaType>()
        list.add(MediaType.APPLICATION_JSON_UTF8)
        converter.supportedMediaTypes = list
        converters!!.add(converter)
    }


/*    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>?) {
        super.configureMessageConverters(converters)
        val fastJsonHttpMessageConverter = FastJsonHttpMessageConverter()
        converters!!.add(fastJsonHttpMessageConverter)
    }*/
    // 添加自定义 消息转换器，返回给浏览器的数据
    // 设置字符串默认 以UTF-8编码返回数据， 设置gson序列化数据到前端
 /*   override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>?) {
        super.configureMessageConverters(converters)
        val stringConverter = StringHttpMessageConverter(Charset.forName("UTF-8"))
        converters!!.add(stringConverter)
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").setPrettyPrinting().create()
        val gsonHttpMessageConverter = GsonHttpMessageConverter()
        gsonHttpMessageConverter.gson = gson
        converters.add(gsonHttpMessageConverter)
    }*/
}