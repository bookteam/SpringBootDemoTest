package net.ftzcode.entity

import javax.persistence.*
import java.util.Date
enum class Status(val value: String) {
    ENABLE("启用"),
    DISABLE("禁用"),
    FREEZE("冻结")
}
enum class Sex(val value: String) {
    MALE("男"),
    FEMALE("女")
}

enum class RoleType(val value: String) {
    ADMIN("总管理员"),
    NORMAL_ADMIN("普通管理员"),
    VIP_USER("VIP用户"),
    NORMAL_USER("普通用户")
}
@Entity
data class User(
        @Id
        @GeneratedValue
        var id: Int = 0,
        @Column(nullable = false, length = 64)
        var token: String? = null,
        @Column(nullable = false, length = 11)
        var phone: String? = null,
        @Column(nullable = false, length = 64)
        var email: String? = null,
        @Column(nullable = false, length = 16)
        var password: String? = null,
        @Column(nullable = false, length = 8)
        var nickName: String? = null,
        var age: Int = 0,
        @Column(nullable = false)
        var role: RoleType? = null,
        var sex: Sex? = null,
        var status: Status? = null,
        @Temporal(TemporalType.TIMESTAMP)
        var createTime: Date? = null,
        @Temporal(TemporalType.TIMESTAMP)
        var updateTime: Date? = null
)
