package net.ftzcode.dao

import net.ftzcode.entity.User
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Created by MAC on 2017/5/7.
 */


interface UserRepository : JpaRepository<User, Int> {
    fun save(user: User): User

    fun findTop1ByToken(token: String):User
}