package net.ftzcode.controller

import net.ftzcode.component.JsonResult
import net.ftzcode.service.UserService
import org.apache.shiro.SecurityUtils
import org.apache.shiro.subject.Subject
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import javax.servlet.http.HttpServletResponse
import org.springframework.web.bind.annotation.ModelAttribute
import java.util.HashMap
import javax.servlet.http.HttpServletRequest


/**
 * Created by WangZiHe on 2017/11/5
 * QQ/WeChat:648830605
 * QQ-Group:368512253
 * Blog:www.520code.net
 * Github:https://github.com/yancheng199287
 */
open class BaseController {

    protected var subject: Subject? = null
    protected var logger = LoggerFactory.getLogger(javaClass)
    protected var data: MutableMap<Any, Any> = HashMap()
    protected lateinit var request: HttpServletRequest
    protected lateinit var response: HttpServletResponse

    @Autowired
    private lateinit var userService: UserService

    @ModelAttribute
    fun setReqAndRes(request: HttpServletRequest, response: HttpServletResponse) {
        this.request = request
        this.response = response
        subject = SecurityUtils.getSubject()
        data.clear()
    }

    fun setOKResult(): JsonResult.Result {
        return JsonResult.getSuccessResult()
    }

    fun setOKResult(data: Any): JsonResult.Result {
        return JsonResult.getSuccessResult(data)
    }

    fun setFailedResult(message: String): JsonResult.Result {
        return JsonResult.getFailResult(message)
    }


}
