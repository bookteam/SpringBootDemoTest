package net.ftzcode.controller.user

import net.ftzcode.controller.BaseController
import net.ftzcode.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController


/**
 * Created by MAC on 2017/5/7.
 */

@RestController
open class UserController : BaseController() {


    @Autowired
    private lateinit var userService: UserService

    @RequestMapping("/index")
    @ResponseBody
    fun index(): Any {
        logger.info("you call me!")
        return setOKResult("welcome to my index of controller!")
    }


}