package net.ftzcode.component


/**
 * Created by WangZiHe on 2017/11/5
 * QQ/WeChat:648830605
 * QQ-Group:368512253
 * Blog:www.520code.net
 * Github:https://github.com/yancheng199287
 */

object JsonResult {

    enum class Status(val code: Int, val desc: String) {
        OK(200, ""),
        FAILED(400, ""),
        SERVER_ERROR(500, "接口错误"),
        NOT_FOUND(404, "接口未找到"),
        FORBIDDEN(403, "禁止访问"),
        UNAUTHORIZED(401, "未经授权")
    }

    fun getSuccessResult(): Result {
        return Result(Status.OK.code, Status.OK.desc, null)
    }

    fun getSuccessResult(data: Any): Result {
        return Result(Status.OK.code, Status.OK.desc, data)
    }

    fun getFailResult(message: String): Result {
        return Result(Status.FAILED.code, message, null)
    }

    fun getFailResult(status: Status, message: String): Result {
        return Result(status.code, message, null)
    }

    //注意对于FastJson 必须要有空的构造方法，也就是在这里的属性必须赋默认值并且访问权限是protected以上，Gson不需要
    data class Result(var code: Int? = 400, var msg: String? = null, var data: Any? = null)

}