package net.ftzcode

import net.ftzcode.service.UserService
import net.ftzcode.util.SpringUtils
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.junit4.SpringRunner


/**
 * Created by WangZiHe on 2017年9月30日 QQ/WeChat:648830605 QQ-Group:368512253
 * Blog:www.520code.net Github:https://github.com/yancheng199287
 */

@RunWith(SpringRunner::class)
@SpringBootTest
class SpringTest {

    @Autowired
    lateinit var ctx: ApplicationContext


    @Test
    fun test001() {
        val userService = SpringUtils.getBean<UserService>("userService")
        print(userService)
    }


}
